Name:		python-pytest-runner
Version:	6.0.1
Release:	1
Summary:	Provide setup.py test support for pytest runner
License:	MIT 
URL:		https://github.com/pytest-dev/pytest-runner/
Source0:	https://files.pythonhosted.org/packages/source/p/pytest-runner/pytest-runner-%{version}.tar.gz
BuildArch:	noarch

%description
Setup scripts can use pytest-runner to add setup.py test support
for pytest runner.

%package -n python3-pytest-runner
Summary:	Provide setup.py test support for pytest runner
Provides:	python-pytest-runner
Requires:	python3-pytest
BuildRequires:	python3-devel 
BuildRequires:	python3-setuptools 
BuildRequires:  python3-pip python3-wheel
BuildRequires:  python3-hatchling python3-hatch-vcs 

%description -n python3-pytest-runner
Setup scripts can use pytest-runner to add setup.py test support
for pytest runner.

%prep
%autosetup -n pytest-runner-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-pytest-runner
%doc README.rst LICENSE
%{python3_sitelib}/*

%changelog
* Fri Sep 20 2024 yaoxin <yao_xin001@hoperun.com> - 6.0.1-1
- Update to 6.0.1:
  * Updated Trove classifier to indicate this project is inactive.

* Wed Aug 23 2023 liyanan <thistleslyn@163.com> - 6.0.0-3
- Adapting to the pyproject.toml compilation mode

* Mon Feb 27 2023 wangkai <wangkai385@h-partners.com> - 6.0.0-2
- Modify the patching method

* Wed Oct 19 2022 wangjunqi <wangjunqi@kylinos.cn> - 6.0.0-1
- Update package to version 6.0.0

* Wed Jun 29 2022 caodongxia <caodongxia@h-partners.com> - 5.3.2-1
- Update to 5.3.2

* Fri Sep 11 2020 wangyue<wangyue92@huawei.com> - 4.0-6
- Remove python2-pytest-runner

* Thu Mar 12 2020 likexin <likexin4@huawei.com> - 4.0-5
- package init
